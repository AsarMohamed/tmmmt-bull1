import { Processor,OnQueueEvent, Process,BullQueueEvents } from 'nest-bull';
import { Job } from 'bull';
import { AppService } from './app.service';

@Processor({ name: 'server A' })
export class App1Processor {
  constructor(private readonly appService: AppService) {}

  @Process({ name: 'print' })
  print(job: Job<number>) {
    console.log('bull 1 server',job.data)
    console.log(`${this.appService.getHello()} : ${job.data}`);
  }
  @Process({ name: 'print_rt_job' })
  print_rt_job(job: Job<number>) {
    console.log('bull 1 job number',job.data)
    console.log(`${this.appService.getHello()} : ${job.data}`);
  }
  // @OnQueueEvent(BullQueueEvents.COMPLETED)
  // onCompleted(job: Job) {
  //   console.log('bull 1 Completed',job.data)
    // this.logger.log(
    //   `Completed job ${job.id} of type ${job.name} with result ${job.returnvalue}`,
    // );
  // }
}