import {Body, Controller, Get,Module, Param, Post } from '@nestjs/common';
import {DoneCallback, Job, Queue} from 'bull';
import {BullModule, InjectQueue} from 'nest-bull';

@Controller()
export class AppController {
 
  constructor(
    @InjectQueue('server A') readonly queue: Queue,
  ) {}
 
  @Get()
  async addJob( @Body() value: any ) {
    // console.log('888888888888888888888888888888888888',this.queue)
    const job: Job = await this.queue.add('print_rt_job', 3);
    return job.id;
  }
}
