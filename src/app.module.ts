import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Queue } from 'bull';

import { App1Processor } from './processor';
import { Module, Logger } from '@nestjs/common';
import { BullModule, InjectQueue } from 'nest-bull';
 
@Module({
  imports: [
    BullModule.register({
      name: 'server A',
      options: {
        redis: {
          port: 6379,
        },
      },
    }) 
  ],
  controllers: [
        AppController,
      ],
  providers: [AppService,App1Processor],
})
export class AppModule {
  private readonly logger = new Logger(AppModule.name, true);
  constructor(
    @InjectQueue('server A') readonly queue: Queue,
  ) { }

  onModuleInit() {

    this.queue.add('print', { currency: 'USD1' }, {
      repeat: { cron: '*/2 * * * *'},
      // removeOnFail: false,
      removeOnComplete: true,
      jobId: 'print-scheduler'
    }).then(job => {
      this.logger.log(`Job ${job.name} successfully started and will run every 2 minute`)
    });

    this.queue.on('completed', (job, result) => {
      this.logger.log(`Job ${job.name} completed! Result: ${result}`);
    });

    this.queue.on('failed', (job, err) => {
      this.logger.error(err)
    })

}
}